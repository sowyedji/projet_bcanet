# encoding: utf8
from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Utilisateur',
            fields=[
                (u'id', models.AutoField(verbose_name=u'ID', serialize=False, auto_created=True, primary_key=True)),
                ('user', models.OneToOneField(to=settings.AUTH_USER_MODEL, to_field=u'id')),
                ('capital', models.IntegerField(null=True)),
                ('chiffre_affaire', models.IntegerField(null=True)),
                ('date_inscription', models.DateField(null=True, blank=True)),
                ('adresse', models.CharField(max_length=100)),
                ('secteur_activite', models.CharField(max_length=100)),
                ('Client', models.BooleanField(default=False)),
                ('Lettre', models.BooleanField(default=False)),
                ('formation', models.BooleanField(default=False)),
                ('est_valider', models.BooleanField(default=False)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Assistance',
            fields=[
                (u'id', models.AutoField(verbose_name=u'ID', serialize=False, auto_created=True, primary_key=True)),
                ('theme', models.CharField(max_length=30)),
                ('intitule_assistance', models.CharField(max_length=30)),
                ('date_publication', models.DateField(null=True, blank=True)),
                ('utilisateur', models.ManyToManyField(to='app.Utilisateur', null=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Lettre_Info',
            fields=[
                (u'id', models.AutoField(verbose_name=u'ID', serialize=False, auto_created=True, primary_key=True)),
                ('themes', models.CharField(max_length=30)),
                ('date_publication', models.DateField(null=True, blank=True)),
                ('montant_lettre', models.IntegerField(null=True)),
                ('numero', models.IntegerField(null=True)),
                ('fichier', models.FileField(null=True, upload_to='/home/sofware/site_bca/app/')),
                ('utilisateur', models.ManyToManyField(to='app.Utilisateur', null=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Modele_Acte',
            fields=[
                (u'id', models.AutoField(verbose_name=u'ID', serialize=False, auto_created=True, primary_key=True)),
                ('type_societe', models.CharField(max_length=30)),
                ('montant_modele', models.IntegerField(null=True)),
                ('date_sortie', models.DateField(null=True, blank=True)),
                ('utilisateur', models.ManyToManyField(to='app.Utilisateur', null=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Session_Formation',
            fields=[
                (u'id', models.AutoField(verbose_name=u'ID', serialize=False, auto_created=True, primary_key=True)),
                ('theme', models.CharField(max_length=30)),
                ('montant_session', models.IntegerField(null=True)),
                ('nombre_max_personne', models.IntegerField(null=True)),
                ('date_debut', models.DateField(null=True, blank=True)),
                ('date_paiement', models.DateField(null=True, blank=True)),
                ('nom_responsable', models.CharField(max_length=30)),
                ('utilisateur', models.ManyToManyField(to='app.Utilisateur', null=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Test_Juridique',
            fields=[
                (u'id', models.AutoField(verbose_name=u'ID', serialize=False, auto_created=True, primary_key=True)),
                ('theme', models.CharField(max_length=30)),
                ('montant_test', models.IntegerField(null=True)),
                ('utilisateur', models.ManyToManyField(to='app.Utilisateur', null=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
