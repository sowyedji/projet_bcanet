from django.shortcuts import render,render_to_response,RequestContext
from models import *
from django.http import HttpResponseRedirect,Http404
from django.contrib.auth import logout,login,authenticate
from django import forms
from forms import formInscription,formConnection,formLettre
from django.contrib.auth.models import User
from datetime import datetime
from app.models import *
from django.contrib.auth.decorators import login_required
from django.core.exceptions import   ObjectDoesNotExist
# Create your views here.
def index(request):
	return render(request,'index.html',locals())#on commence a partir de template_dir
#deconnection
def deconnection_page(request):
	logout(request)
	return HttpResponseRedirect('/')
#view inscription
def inscription(request):
	if request.method=='POST':
		form_inscription=formInscription(request.POST)
		if  form_inscription.is_valid():
			user=User.objects.create_user(username=form_inscription.cleaned_data['username'],password=form_inscription.cleaned_data['password'],email=form_inscription.cleaned_data['email'])
			#creation du profile
			utilisateur=Utilisateur(user=user,capital=form_inscription.cleaned_data['capital'],chiffre_affaire=form_inscription.cleaned_data['chiffre'],adresse=form_inscription.cleaned_data['adresse'],secteur_activite=form_inscription.cleaned_data['secteur'])
			#insertion dans la base
			utilisateur.save()
			return render(request,'index.html')
	else:
		form_inscription=formInscription()
	return render(request,'inscription.html',locals())
#view vers la page de connection
def authentification(request):
	error=False
	if request.method=='POST':
		form_connection=formConnection(request.POST)
		if form_connection.is_valid():
			username=form_connection.cleaned_data['username']
			password=form_connection.cleaned_data['password']
			user=authenticate(username=username,password=password)
			if user is not None:
				#recuperation de l'utilisateur
				try:
					utilisateur=Utilisateur.objects.get(user=user)
				except ObjectDoesNotExist:
					raise Http404('Le compte n existe pas,veuillez vous inscrire')
				if utilisateur.Client==True:
					#Mise en session
					login(request,user)
					#redirection vers la page de succes des Client
					return render(request, 'succesClients.html',locals())
				else:
					if utilisateur.Lettre==True:
						#Mise en session
						login(request,user)
						#redirection vers la page de succes des Abonnees aux lettres
						return render(request, 'succesAbonnes.html',locals())
					else:
						if user.is_superuser:
							login(request,user)
							#redirection vers la page de succes Admin
							return render(request, 'succesAdmin.html',locals())
			else:
				error=True
				return render_to_response('connection.html',locals(),context_instance=RequestContext(request))
	else:
		form_connection=formConnection()
	return render(request,'connection.html',{'form_connection':form_connection})
#Creation de lettres
def uploader_lettre(request):
	sauvegarde=False
	if request.method=='POST':
		form=formLettre(request.POST, request.FILES)
		if form.is_valid():
			lettre_Info=Lettre_Info()
			lettre_Info.intitule=form.cleaned_data['themes']
			lettre_Info.montant_lettre=form.cleaned_data['montant_lettre']
			lettre_Info.date_publication=datetime.today()
			lettre_Info.numero=form.cleaned_data['numero']
			lettre_Info.fichier=form.cleaned_data['fichier']
			lettre_Info.save()
			sauvegarde=True
			#return succes page
			return render(request,'uploader_lettre.html',locals())
	else:
		form=formLettre()
	return render(request,'uploader_lettre.html',locals())






		


	
