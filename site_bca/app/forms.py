from django import forms
from django.core.exceptions import ObjectDoesNotExist
from django.contrib.auth.models import User
#formulaire d'inscription 
class formInscription(forms.Form):
	societe=forms.CharField(label=" Votre Societe")
	username=forms.CharField(label="Username")
	email=forms.EmailField(label=" Email")
	password=forms.CharField(widget=forms.PasswordInput,label="Password")
	confirmer=forms.CharField(widget=forms.PasswordInput,label="confirm your Password")
	capital=forms.IntegerField(label="capital Social")
	chiffre=forms.IntegerField(label="chiffre d'affaire")
	adresse=forms.CharField(widget=forms.Textarea)
	secteur=forms.CharField(max_length=100,label="Secteur d'activite")
	#Validation des champs
	
#formulaire authentification
class formConnection(forms.Form):
	username=forms.CharField(label="Username")
	password=forms.CharField(widget=forms.PasswordInput,label="Password")

#formulaire de creation des lettres d'informations
class formLettre(forms.Form):
	montant_lettre=forms.IntegerField(label="Montant de la lettre")
	numero=forms.IntegerField(label="Numero de la lettre")
	themes=forms.CharField(widget=forms.Textarea)
	#fichier pdf
	fichier=forms.CharField(widget=forms.FileInput)

