from django.conf.urls import patterns, include, url
from django.contrib import admin
from app.views import index
admin.autodiscover()

urlpatterns = patterns('app.views',
	
    url(r'^admin/', include(admin.site.urls)),	
    url(r'^bca-pme/$', 'index'),
    url(r'^accueil/$','index'),
    url(r'^inscription/$','inscription'),
    url(r'^deconnexion/$','index'),
    url(r'^connection/$','authentification'),
    url(r'^uploaderlettre/$','uploader_lettre'),
    )



