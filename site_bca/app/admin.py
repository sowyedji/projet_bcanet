from django.contrib import admin
from app.models import *
# Register your models here.
admin.site.register(Session_Formation)
admin.site.register(Test_Juridique)
admin.site.register(Modele_Acte)
admin.site.register(Lettre_Info)
admin.site.register(Assistance)
admin.site.register(Utilisateur)