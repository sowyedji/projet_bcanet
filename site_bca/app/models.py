from django.db import models	
from django.contrib.auth.models import User
## Mes modeles

#utilisateur
class Utilisateur(models.Model):
	user=models.OneToOneField(User)
	capital=models.IntegerField(null=True)
	chiffre_affaire=models.IntegerField(null=True)
	date_inscription=models.DateField(blank=True, null=True)
	adresse=models.CharField(max_length=100)
	secteur_activite=models.CharField(max_length=100)
	Client=models.BooleanField(default=False)
	Lettre=models.BooleanField(default=False)
	formation=models.BooleanField(default=False)
	est_valider=models.BooleanField(default=False)#pas besoins de ca pour la validation
	def __unicode__(self):
		return self.user.username
#session de fomation
class Session_Formation(models.Model):
	theme=models.CharField(max_length=30)
	montant_session=models.IntegerField(null=True)
	nombre_max_personne=models.IntegerField(null=True)
	date_debut=models.DateField(blank=True, null=True)
	date_paiement=models.DateField(blank=True, null=True)
	nom_responsable=models.CharField(max_length=30)
	#fichier pdf
	utilisateur=models.ManyToManyField(Utilisateur,null=True)
	def __unicode__(self):
		return self.theme

#testJuridique
class Test_Juridique(models.Model):
	theme=models.CharField(max_length=30)
	montant_test=models.IntegerField(null=True)
	#fichier pdf
	utilisateur=models.ManyToManyField(Utilisateur,null=True)
	def __unicode__(self):
		return self.theme

#modeleActe
class Modele_Acte(models.Model):
	type_societe=models.CharField(max_length=30)
	montant_modele=models.IntegerField(null=True)
	date_sortie=models.DateField(blank=True, null=True)
	#fichier pdf
	utilisateur=models.ManyToManyField(Utilisateur,null=True)
	def __unicode__(self):
		return self.type_societe

#lettre d'informations
class Lettre_Info(models.Model):
	intitule=models.CharField(max_length=30)
	date_publication=models.DateField(blank=True, null=True)
	montant_lettre=models.IntegerField(null=True)
	numero=models.IntegerField(null=True)
	#fichier pdf
	fichier = models.FileField(upload_to="/home/sofware/site_bca/app/",null=True)
	utilisateur=models.ManyToManyField(Utilisateur,null=True)
	def __unicode__(self):
		return self.intitule

#Assistance
class Assistance(models.Model):
	theme=models.CharField(max_length=30)
	intitule_assistance=models.CharField(max_length=30)
	date_publication=models.DateField(blank=True, null=True)
	utilisateur=models.ManyToManyField(Utilisateur,null=True)
	def __unicode__(self):
		return self.theme





